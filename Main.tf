
provider "aws" {
  region = "us-east-1"
}

/// sns topic configuration

resource "aws_sns_topic" "aws-connect-alarms-topic2" {
  name = "aws-connect-alarm-topic2"
}

/// sns topic subscription configuration

resource "aws_sns_topic_subscription" "aws-connect-topic-sub2" {
  topic_arn              = aws_sns_topic.aws-connect-alarms-topic2.arn
  protocol               = "email"
  endpoint               = "oawujo@gmail.com"
  endpoint_auto_confirms = "true"
}

/// cloudwatch configuration

resource "aws_cloudwatch_metric_alarm" "aws-connect-queued_calls2" {
  alarm_name                = "aws-connect-queued-calls2"
  comparison_operator       = "GreaterThanOrEqualToThreshold"
  evaluation_periods        = "1"
  metric_name               = "LongestQueueWaitTime"
  namespace                 = "AWS/Connect"
  period                    = "60"
  threshold                 = "10"
  statistic                 = "Average"
  alarm_description         = "This alarm monitors the longest call in the queue"
  alarm_actions             = [aws_sns_topic.aws-connect-alarms-topic2.arn]
  insufficient_data_actions = []
  dimensions = {
    InstanceId  = "0acd9f71-9f15-4841-8c49-e2e0a04b6408"
    MetricGroup = "Queue"
    QueueName   = "OK_Test_Queue"
  }
}